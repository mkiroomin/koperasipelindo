
import { combineReducers, } from "redux";
import { createSelectorHook, } from "react-redux";
import { createReducer, createAction, } from '@reduxjs/toolkit'
import produce from "immer"

const initialState = {
    valOrder: 0,
};

const Cart = (state = initialState, action) => {

    if (action.type === 'DEFAULT_ORDER') {

        return {
            ...state,
            valOrder: action.val
        }

    }

    if (action.type === 'PLUS_ORDER') {
        return {
            ...state,
            valOrder: state.valOrder + 1
        }
    }

    if (action.type === 'MIN_ORDER') {
        let totalOrder = 1;
        if (state.valOrder > 1) {
            return {
                ...state,
                valOrder: state.valOrder - 1
            }
        }
        return {
            ...state,
            valOrder: totalOrder
        }
    }

    return state;
}


const initialStateCartCount = {
    cartCount: 0,
    cartTotal: 0,
}

const CartCount = (state = initialStateCartCount, action) => {
    if (action.type === 'SET_CART_COUNT') {
        return {
            ...state,
            cartCount: action.cartCount,
            cartTotal: action.cartTotal,
        }
    }
    return state;
}

const InitialCompanyData = {
    companyData: [],
};

const CompanyData = (state = InitialCompanyData, action) => {
    if (action.type === 'SET_COMPANY_DATA') {
        return {
            ...state,
            companyData: action.companyData,
        }
    }
    return state;
}

const InitialProductList = {
    productList: [],
};

const ProductList = (state = InitialProductList, action) => {
    if (action.type === 'SET_PRODUCT_LIST') {
        return {
            ...state,
            productList: action.productList,
        }
    }
    return state;
}


const initialUserData = {
    userData: [],

}

const UserData = (state = initialUserData, action) => {
    if (action.type === 'SET_USER_DATA') {
        return {
            ...state,
            userData: action.userData,
        }
    }
    return state;
}

const initialRegionAvailability = {
    regionAvailability: null,

}

const RegionAvailability = (state = initialRegionAvailability, action) => {
    if (action.type === 'SET_REGION_AVAILABILITY') {
        return {
            ...state,
            regionAvailability: action.regionAvailability,
        }
    }
    return state;
}

const initialIsChangeNominal = {
    isChangeNominal: false,

}

const IsChangeNominal = (state = initialIsChangeNominal, action) => {
    if (action.type === 'SET_IS_CHANGE_NOMINAL') {
        return {
            ...state,
            isChangeNominal: action.isChangeNominal,
        }
    }
    return state;
}

const initialStateDetailCart = {
    detailCart: []
}


// const DetailCart = (state = initialStateDetailCart, action) => {
//     if (action.type === "SET_DETAIL_CART") {
//         return {
//             ...state,
//             detailCart: action.detailCart,
//         }
//     }
//     if (action.type === "PUSH_DETAIL_CART") {
//         return {
//             ...state,
//             detailCart: [...state.detailCart, action.detailCart]
//         }
//     }
//     if (action.type === "UPDATE_DETAIL_CART") {
//         return {
//             ...state,
//             detailCart: [
//                 ...state.detailCart.slice(0, action.index),
//                 { ...state.detailCart[action.index], jumlah_pesanan: action.jumlahPesanan },
//                 ...state.detailCart.slice(action.index + 1)
//             ]
//         }
//     }
//     return state;
// }


const DetailCart = (state = initialStateDetailCart, action) =>
    produce(state, draft => {
        switch (action.type) {
            case 'SET_DETAIL_CART':
                draft.detailCart = action.detailCart;
                break;
            case 'PUSH_DETAIL_CART':
                draft.detailCart.push(action.detailCart);
                break;
            case 'UPDATE_DETAIL_CART':
                draft.detailCart[action.index].jumlah_pesanan = action.jumlahPesanan;
                break;
            default:
                break;
        }

    })

// const cartSelector = initialStateDetailCart => initialStateDetailCart.produk_id;
// export const getCartSelector = createSelectorHook(cartSelector, produk_id => Object.values(produk_id));

const initialStateDataCart = {
    dataCart: []
}

const DataCart = (state = initialStateDataCart, action) => {
    if (action.type === "SET_DATA_CART") {
        return {
            ...state,
            dataCart: action.dataCart,
        }
    }
    return state;
}

const reg = {
    jumlah: {
        jumlah: 0,
    },
    jumlahDefault: 0,
    id: null,
};

const Reg = (state = reg, action) => {
    if (action.type === 'TAMBAH_ORDER') {
        return {
            ...state,
            jumlah: {
                [action.ids]: action.val,
            },
            jumlahDefault: 1,
            id: action.ids,
        }

    }

    return state;
}

const initialStateToastAlert = {
    isShowing: false,
    alertLabel: '',
};

const ToastAlert = (state = initialStateToastAlert, action) => {
    if (action.type === 'SET_TOAST_ALERT') {
        return {
            ...state,
            isShowing: action.isShowing,
            alertLabel: action.alertLabel,
        }

    }

    return state;
}

const buahreducer = {
    jumlah: {
        jumlah: 0,
    },
    jumlahDefault: 0,
    id: null,
};

const Buahreducer = (state = buahreducer, action) => {
    if (action.type === 'TAMBAH_ORDER_BUAH') {

        return {
            ...state,
            jumlah: {
                [action.ids]: action.val,
            },
            jumlahDefault: 1,
            id: action.ids,
        }

    }

    return state;
}

const telurreducer = {
    jumlah: {
        jumlah: 0,
    },
    jumlahDefault: 0,
};

const Telurreducer = (state = telurreducer, action) => {
    if (action.type === 'TAMBAH_ORDER_TELUR') {

        return {
            ...state,
            jumlah: {
                [action.ids]: action.val,
            },
            jumlahDefault: 1,
        }

    }

    return state;
}


const updateHarga = {
    data: {
        data: 0,
    },
};

const UpdateHarga = (state = updateHarga, action) => {
    if (action.type === 'UPDATE_HARGA') {

        return {
            ...state,
            data: {
                [action.ids]: action.value * action.produk_harga,
            },
        }

    }

    return state;
}

const saldo = {
    jumlahSaldo: 0,
};

const Saldo = (state = saldo, action) => {
    if (action.type === 'SALDO') {

        return {
            ...state,
            jumlahSaldo: action.jumlahSaldo
        }
    }

    return state;
}

const notif = {
    notif: 0,
    isLoading: false,
};

const Notif = (state = notif, action) => {
    if (action.type === 'NOTIF') {

        return {
            ...state,
            notif: action.notif,
            isLoading: action.isLoading
        }
    }

    return state;
}

const alamat = {
    alamat: '',
    kirim: '',
    latitude: '',
    longitude: '',
};

const Alamat = (state = alamat, action) => {
    if (action.type === 'TAMBAH_ALAMAT') {

        return {
            ...state,
            alamat: action.alamat,
            kirim: action.kirim,
            latitude: action.latitude,
            longitude: action.longitude,
        }
    }

    return state;
}

const alamatAgent = {
    dataAlamatAgent: [],
    indexSelected: null,
};

const AlamatAgent = (state = alamatAgent, action) => {
    if (action.type === 'SET_ALAMAT_AGENT') {
        return {
            ...state,
            dataAlamatAgent: action.dataAlamatAgent,
            indexSelected: action.indexSelected,
        }
    }

    return state;
}

const voucher = {
    dataVoucher: [],
    indexSelected: null,
    totalBayar: 0,
};

const Voucher = (state = voucher, action) => {
    // console.log('action=>', action)
    if (action.type === 'SET_VOUCHER') {
        return {
            ...state,
            dataVoucher: action.dataVoucher,
            indexSelected: action.indexSelected,
            totalBayar: action.totalBayar
        }
    }

    return state;
}

const dataProduk = {
    produkAll: null,
};

const DataProduk = (state = dataProduk, action) => {
    if (action.type === 'PRODUK') {

        return {
            ...state,
            produkAll: action.produkAll
        }
    }

    return state;
}

const idPrdk = {
    idProduk: 0,
};

const IdProduk = (state = idPrdk, action) => {
    if (action.type === 'SET_ID_PRODUK') {
        return {
            ...state,
            idProduk: action.idProduk
        }
    }
    return state;
}


const initialSelectedArea = {
    selectedArea: [],
    indexSelectedArea: 0,
};

const SelectedArea = (state = initialSelectedArea, action) => {
    if (action.type === 'SET_AREA') {
        return {
            ...state,
            selectedArea: action.selectedArea,
            indexSelectedArea: action.indexSelectedArea
        }
    }
    return state;
}



const initialSelectedBank = {
    selectedBank: [],
    indexSelectedBank: 0,
};

const SelectedBank = (state = initialSelectedBank, action) => {
    if (action.type === 'SET_BANK') {
        return {
            ...state,
            selectedBank: action.selectedBank,
            indexSelectdBank: action.indexSelectedBank
        }
    }
    return state;
}


const initialNotificationData = {
    notificationData: [],
    notifCount: null,
};
const NotificationData = (state = initialNotificationData, action) => {
    if (action.type === 'SET_NOTIFICATION_DATA') {
        return {
            ...state,
            notificationData: action.notificationData,
            notifCount: action.notifCount,
        }
    }
    return state;
}

const InitialNewTransactions = {
    newTransactions: [],
    trxCount: null,
};

const NewTransactions = (state = InitialNewTransactions, action) => {
    if (action.type === 'SET_NEW_TRANSACTIONS') {
        return {
            ...state,
            newTransactions: action.newTransactions,
            trxCount: action.trxCount,
        }
    }
    return state;
}


const InitialCategoryData = {
    categoryData: [],
    selectedCategory: [],
    indexSelectedCategory: 0,
};

const CategoryData = (state = InitialCategoryData, action) => {
    if (action.type === 'SET_CATEGORY_DATA') {
        return {
            ...state,
            categoryData: action.categoryData,
        }
    }
    if (action.type === 'SET_SELECTED_CATEGORY') {
        return {
            ...state,
            selectedCategory: action.selectedCategory,
            indexSelectedCategory: action.indexSelectedCategory,
        }
    }
    return state;
}


const InitialFlashSaleCategory = {
    flashSaleCategory: null,
};

const FlashSaleCategory = (state = InitialFlashSaleCategory, action) => {
    if (action.type === 'SET_FLASHSALE_CATEGORY') {
        return {
            ...state,
            flashSaleCategory: action.flashSaleCategory,
        }
    }
    return state;
}


const initialRouteRootStack = {
    initialRouteName: null,
};

const InitialRouteRootStack = (state = initialRouteRootStack, action) => {
    if (action.type === 'SET_INITIAL_ROUTE_NAME') {
        return {
            ...state,
            initialRouteName: action.initialRouteName,
        }
    }
    return state;
}


const initialBackgroundHeader = {
    backgroundHeader: null,
    // backgroundHeader: '#FFF5E2',
};

const BackgroundHeader = (state = initialBackgroundHeader, action) => {
    if (action.type === 'SET_BACKGROUND_HEADER') {
        return {
            ...state,
            backgroundHeader: action.backgroundHeader,
        }
    }
    return state;
}

const initialHalalPoint = {
    halalPoint: 0,
};

const HalalPoint = (state = initialHalalPoint, action) => {
    if (action.type === 'SET_HALALPOINT') {
        return {
            ...state,
            halalPoint: action.halalPoint,
        }
    }
    return state;
}


const initialActiveTag = {
    indexActiveTag: 0,
    itemActiveTag: [],
};

const ActiveTag = (state = initialActiveTag, action) => {
    if (action.type === 'SET_ACTIVE_TAG') {
        return {
            ...state,
            indexActiveTag: action.indexActiveTag,
            itemActiveTag: action.itemActiveTag,
        }
    }
    return state;
}


const initialSelectedDate = {
    indexSelectedDate: null,
    selectedDate: {},
};

const SelectedDate = (state = initialSelectedDate, action) => {
    if (action.type === 'SET_SELECTED_DATE') {
        return {
            ...state,
            selectedDate: action.selectedDate,
            indexSelectedDate: action.indexSelectedDate,
        }
    }
    return state;
}


const initialDeliveryDateList = {
    deliveryDateList: [],
};

const DeliveryDateList = (state = initialDeliveryDateList, action) => {
    if (action.type === 'SET_DELIVERY_DATE_LIST') {
        return {
            ...state,
            deliveryDateList: action.deliveryDateList,
        }
    }
    return state;
}

const initialIsMaintenance = {
    isMaintenance: false,
};

const IsMaintenance = (state = initialIsMaintenance, action) => {
    if (action.type === 'SET_IS_MAINTENANCE') {
        return {
            ...state,
            isMaintenance: action.isMaintenance,
        }
    }
    return state;
}


const reducer = combineReducers({
    ProductList,
    Cart,
    CartCount,
    CompanyData,
    IsChangeNominal,
    DetailCart,
    DataCart,
    Reg,
    Buahreducer,
    Telurreducer,
    UpdateHarga,
    Saldo,
    Notif,
    Alamat,
    AlamatAgent,
    DataProduk,
    Voucher,
    UserData,
    IdProduk,
    InitialRouteRootStack,
    SelectedArea,
    SelectedBank,
    NotificationData,
    NewTransactions,
    RegionAvailability,
    CategoryData,
    ToastAlert,
    FlashSaleCategory,
    BackgroundHeader,
    HalalPoint,
    ActiveTag,
    SelectedDate,
    DeliveryDateList,
    IsMaintenance,
})

export default reducer;