/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import { NavigationContainer, DefaultTheme as NavigationDefaultTheme, DarkTheme as NavigationDarkTheme, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator, NativeStackNavigationOptions } from '@react-navigation/native-stack';
import { AuthContext } from './components/context';
import { Provider, connect } from 'react-redux';
import { store } from './src/redux';
import { enableScreens } from 'react-native-screens';
import AsyncStorage from '@react-native-async-storage/async-storage';

//import root
import RootStackScreen from './screens/RootStackScreen';
import HomeRootStackScreen from './screens/HomeRootStackScreen';

enableScreens(true);
const Stack = createNativeStackNavigator();


const App = (props) => {

  const [username, setUsername] = useState(null);
  const [mail, setEmail] = useState(null);

  useEffect(() => {
    getUserData();
  }, [])

  const getUserData = () => {
    (async () => {
      let userName;
      let email;
      userName = await AsyncStorage.getItem('@name');
      email = await AsyncStorage.getItem('@email');
      console.log(userName, email)
      setUsername(userName);
      setEmail(email);
    })();
  }

  return (
    <Provider store={store} >
      <NavigationContainer>
        {username == null && mail == null ?
            <RootStackScreen /> 
          :
            <HomeRootStackScreen />
        }
      </NavigationContainer>
    </Provider>
  );
};

export default App;
