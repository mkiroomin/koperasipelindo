import React, { useState, useEffect } from 'react';
import {View, Text, TouchableOpacity, TextInput, Platform, StyleSheet, StatusBar, Alert, Image, Linking, Dimensions, ToastAndroid, ImageBackground} from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import helper from "../components/helper";
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useTheme } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import { GoogleSignin, GoogleSigninButton, statusCodes, } from '@react-native-community/google-signin';
import themeStyle from '../styles/theme.style';

const screenWidth = Math.round(Dimensions.get('window').width)
const screenHeight = Math.round(Dimensions.get('window').height)


const SignInScreen = ({ navigation }) => {

    const [data, setData] = React.useState({
        username: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
    });

    const [clikedSignIn, setClikedSignIn] = useState(false)

    const { colors } = useTheme();

    let webClientID = '40286503666-qm2qmupib27mj1d34vcmbsi2452lnoti.apps.googleusercontent.com'

    useEffect(() => {
        GoogleSignin.configure({
            // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
            webClientId: webClientID,
            // '823879768432-koh1mluaan3cch9193okvq7er37sv8tu.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            // hostedDomain: '', // specifies a hosted domain restriction
            // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
            forceCodeForRefreshToken: false, // [Android] related to `serverAuthCode`, read the docs link below *.
            // accountName: '', // [Android] specifies an account name on the device that should be used
            // iosClientId: '953482265689-v00ppkuvmhscg6qj5r5okvtveano0k8j.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
        });

    }, [])


    const textInputChange = (val) => {
        if (helper.validateEmail(val) == true) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: false,
                isValidUser: false
            });
        }
    }

    const handlePasswordChange = (val) => {
        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const [openAlert, setOpenAlert] = useState(false)
    const [alertIcon, setAlertIcon] = useState(null)
    const [alertIconLoading, setAlertIconLoading] = useState(null)
    const [alertTitle, setAlertTitle] = useState("")
    const [alertMessage, setAlertMessage] = useState("")
    const [userInfo, setUserInfo] = useState("")
    const [inputPhone, setInputPhone] = useState(false)
    const [phone, setPhone] = useState("")
    const [nama, setNama] = useState("")
    const [kode, setKode] = useState("")
    const [mail, setMail] = useState("")
    const [id, setId] = useState("")

    const _signIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            // console.log("userInfo.user.name", userInfo.user.name)
            // console.log("userInfo.user.email", userInfo.user.email)
            setUserInfo(userInfo);
            setNama(userInfo.user.name)
            setKode(userInfo.user.id)
            setMail(userInfo.user.email)
            setId(userInfo.user.id)

            AsyncStorage.setItem('@name', userInfo.user.name);
            AsyncStorage.setItem('@email', userInfo.user.email);

            const request = await fetch("https://test1.kopelindo.co.id/api/login", {
                method: 'POST',
                dataType: 'json',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                },
                body: JSON.stringify({
                    username: "admin",
                    password: "123456",
                })
            })

            const response = await request.json()
            // console.log('response REGION POST', response)
            if (response.status == "success") {
                setOpenAlert(false);
                navigation.navigate('HomeScreen')
            }

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                (async () => {
                    try {
                        await GoogleSignin.signOut();
                    } catch (e) {
                        console.error(e);
                    }
                })
            } else if (error.code === statusCodes.IN_PROGRESS) {
                ToastAndroid.showWithGravityAndOffset(
                    error.code,
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                );
                (async () => {

                    try {
                        await GoogleSignin.signOut();
                    } catch (e) {
                        console.error(e);
                    }
                })
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                ToastAndroid.showWithGravityAndOffset(
                    'PLAY_SERVICES_NOT_AVAILABLE',
                    // error.code,
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                );
                (async () => {

                    try {
                        await GoogleSignin.signOut();
                    } catch (e) {
                        console.error(e);
                    }
                })
                // play services not available or outdated
            } else {
                ToastAndroid.showWithGravityAndOffset(
                    error.code,
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    25,
                    50
                );
                (async () => {


                    try {
                        await GoogleSignin.signOut();
                    } catch (e) {
                        console.error(e);
                    }
                })
                // some other error happened
            }
        }
    };


    const sendWA = async () => {
        Linking.openURL('whatsapp://send?text=اَلسَّلَامُ عَلَيْكُمْ\n &phone=6282229038191')
        return;
    }

    const loginManual = async (userName, password) => {
        console.log(userName, password)
        if(userName == 'admin' || password == '123456'){
            setAlertIconLoading(require('../assets/progress.gif'))
            setOpenAlert(true);
            AsyncStorage.setItem('@name', userName);
            AsyncStorage.setItem('@email', userName);
            setOpenAlert(false);

            const request = await fetch("https://test1.kopelindo.co.id/api/login", {
                method: 'POST',
                dataType: 'json',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache',
                },
                body: JSON.stringify({
                    username: "admin",
                    password: "123456",
                })
            })

            const response = await request.json()
            // console.log('response REGION POST', response)
            if (response.status == "success") {
                setOpenAlert(false);
                navigation.navigate('HomeScreen')
            }

        } else {
            if (userName == '' || password == '') {
                setClikedSignIn(true);
            } else {
                setAlertIcon(require("../assets/warning_icon1.png"))
                setAlertMessage('Email: user@gmail.com & pass: 12345678')
                setAlertIconLoading(null)
                setOpenAlert(true);
            }
        }
    }

    return (
        <LinearGradient
            start={{ x: 1, y: 0 }} end={{ x: 1, y: 1 }} colors={['#FFFFFE', '#F1F1F1']}
            style={styles.container}
        >
            <Dialog
                visible={openAlert}
                width={screenWidth / 1.2}
                onTouchOutside={() => {
                    setOpenAlert(false);
                }}
                onHardwareBackPress={() => {
                    setOpenAlert(false);
                    return true;
                }}
            >
                <DialogContent>
                    {alertIconLoading ?
                        <View style={{ alignItems: "center", justifyContent: "center", }}>
                            <View style={{ overflow: "hidden", height: helper.normalize(30), width: helper.normalize(30), alignItems: "center", justifyContent: "center" }}>
                                <Image resizeMode="cover" source={alertIconLoading} style={{ flex: 1, height: helper.normalize(60), width: helper.normalize(100), }} />
                            </View>
                            <Text style={{ fontFamily: themeStyle.POPPINS_REGULAR, fontSize: helper.normalize(14), top: -10 }}>Loading...</Text>
                        </View>
                        :
                        <View style={{ alignItems: "center", justifyContent: "center", padding: 20 }}>
                            <Image source={alertIcon} style={{ height: helper.normalize(50), width: helper.normalize(50) }} />
                            <Text style={{ fontFamily: themeStyle.POPPINS_BOLD, fontSize: helper.normalize(14), paddingTop: 10 }}>{alertTitle}</Text>
                            <Text style={{ textAlign: "center", fontFamily: themeStyle.POPPINS_REGULAR, fontSize: helper.normalize(14), opacity: 0.8, paddingTop: 10 }}>{helper.capitalize(alertMessage)}</Text>
                            {/* <TouchableOpacity onPress={sendWA}>
                                <Image source={require("../assets/whatsapp.gif")} style={{ height: helper.normalize(30), width: helper.normalize(30), marginTop: 40, alignSelf: "center" }} />
                                <Text style={{ fontFamily: themeStyle.POPPINS_BOLD, fontSize: helper.normalize(10), paddingTop: 10 }}>klik untuk hubungi customer service kami</Text>
                            </TouchableOpacity> */}
                        </View>

                    }
                </DialogContent>
            </Dialog>

            <StatusBar barStyle="light-content" translucent={true} />
            {/* <View style={styles.headerContainer}>
                <Image source={require("../assets/boscod_logo.png")} style={{ resizeMode: 'contain', alignSelf: "center", height: helper.normalize(80), width: "75%"}} />
            </View> */}
            <Animatable.View
                animation="slideInUp"
                style={styles.footerContainerPelabuha}
                // style={styles.footerContainer}
            >
                <Text style={{color: 'black', fontSize: helper.normalize(20), fontFamily: themeStyle.POPPINS_MEDIUM}}>Sign In</Text>
                <Text style={{fontSize: helper.normalize(12), fontFamily: themeStyle.POPPINS_MEDIUM, color: 'grey', paddingBottom: helper.normalize(20)}}>Selamat datang kembali</Text>
                <View style={[styles.action, { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
                    <Text style={{ fontFamily: themeStyle.POPPINS_REGULAR, fontSize: helper.normalize(12), color: "#6D6D6D" }} >Username</Text>
                    <View style={{ flexDirection: "row" }} >
                        <TextInput
                            // 
                            placeholder="admin"
                            placeholderTextColor="#CCC"
                            style={[styles.textInput, {
                                color: colors.text
                            }]}
                            // underlineColorAndroid={themeStyle.PRIMARY_COLOR}
                            autoCapitalize="none"
                            textContentType="emailAddress"
                            keyboardType="email-address"
                            onChangeText={(val) => textInputChange(val)}
                        // onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                        />
                        {data.check_textInputChange ?
                            <Animatable.View
                                animation="bounceIn"
                            >
                                <Feather
                                    name="check-circle"
                                    color="green"
                                    size={20}
                                />
                            </Animatable.View>
                            : null}
                    </View>
                    {!data.isValidUser && data.username.length > 0 &&
                        <Animatable.View useNativeDriver={true} animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Username tidak valid.</Text>
                        </Animatable.View>
                    }
                </View>
                <View style={[styles.action, { borderTopWidth: 0, borderTopRightRadius: 0, borderTopLeftRadius: 0 }]}>
                    <Text style={{ fontFamily: themeStyle.POPPINS_REGULAR, fontSize: helper.normalize(12), color: "#6D6D6D" }} >Password</Text>
                    <View style={{ flexDirection: "row" }} >

                        <TextInput

                            placeholder="123456"
                            placeholderTextColor="#CCC"
                            secureTextEntry={data.secureTextEntry ? true : false}
                            style={[styles.textInput, {
                                color: colors.text
                            }]}
                            autoCapitalize="none"
                            onChangeText={(val) => handlePasswordChange(val)}
                        />
                        <TouchableOpacity
                            onPress={updateSecureTextEntry}
                        >
                            {data.secureTextEntry ?
                                <Feather
                                    name="eye-off"
                                    color="grey"
                                    size={helper.normalize(14)}
                                />
                                :
                                <Feather
                                    name="eye"
                                    color="grey"
                                    size={helper.normalize(14)}
                                />
                            }
                        </TouchableOpacity>
                    </View>
                    {data.password == '' && clikedSignIn == true &&
                        <Animatable.View useNativeDriver={true} animation="fadeInLeft" duration={500}>
                            <Text style={styles.errorMsg}>Password tidak boleh kosong.</Text>
                        </Animatable.View>
                    }
                </View>

                <View style={{ paddingTop: 30 }}>
                    <TouchableOpacity
                        onPress={() => { loginManual(data.username, data.password) }}
                        style={[styles.signIn, { backgroundColor: "orange", paddingVertical: helper.normalize(14), borderRadius: helper.normalize(20)}]}
                    >
                        <Text style={[styles.textSign, {
                            color: 'black'
                        }]}>Login</Text>
                    </TouchableOpacity>

                    <Text style={[styles.textSign, {
                        fontFamily: themeStyle.POPPINS_REGULAR,
                        fontSize: helper.normalize(14),
                        color: '#717171',
                        alignSelf: 'center',
                        marginVertical: helper.normalize(10),
                        paddingTop: 10,
                        paddingBottom: 10
                    }]}>atau</Text>

                    <TouchableOpacity
                        style={{ overflow: 'hidden' }}
                        onPress={_signIn}
                        children={
                            <View style={{ backgroundColor: '#FFF', flexDirection: "row", borderWidth: 1, borderColor: '#737373', alignItems: "center", justifyContent: "center", padding: helper.normalize(12), borderRadius: helper.normalize(20) }}>
                                <Image source={require("../assets/google_icon.png")} style={{ height: helper.normalize(20), width: helper.normalize(20) }} />
                                <Text style={[styles.text_footer, {
                                    color: '#2d2d2d',
                                    fontSize: helper.normalize(14),
                                    marginLeft: helper.normalize(10),
                                }]}>Lanjutkan dengan Google</Text>
                            </View>
                        }
                    />
                </View>
            </Animatable.View>
            <View>
                <TouchableOpacity
                    onPress={() => { sendWA() }}
                    style={{ position: "absolute", right: helper.normalize(20), top: helper.normalize(60), backgroundColor: themeStyle.PRIMARY_COLOR, paddingVertical: helper.normalize(10), borderRadius: helper.normalize(30), width:50, alignItems: "center", justifyContent: "center"}}
                >
                    <Ionicons 
                        name="logo-whatsapp" 
                        color="white"
                        size={helper.normalize(30)}/>
                </TouchableOpacity>
            </View>

        </LinearGradient>

    );
};

export default SignInScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerContainer: {
        paddingTop: helper.normalize(100),
        paddingBottom: helper.normalize(50),
        justifyContent: "center",
    },
    footerContainer: {
        paddingHorizontal: helper.normalize(20),
        paddingBottom: helper.normalize(20),
    },
    footerContainerPelabuha: {
        paddingHorizontal: helper.normalize(20),
        paddingBottom: helper.normalize(20),
        paddingTop: helper.normalize(200),
    },
    text_header: {
        paddingTop: 20,
        color: '#fff',
        fontSize: helper.normalize(20),
        fontFamily: themeStyle.POPPINS_SEMIBOLD,
        alignSelf: "center",
    },

    text_footer: {
        color: "#2d2d2d",
        fontSize: helper.normalize(16),
        fontFamily: themeStyle.POPPINS_MEDIUM,
    },
    action: {
        // flexDirection: 'row',
        // marginTop: 10,
        // borderBottomWidth: 1,
        borderWidth: 1,
        backgroundColor: "#FDFDFD",
        borderRadius: helper.normalize(6),
        borderColor: "#BBBBBB",
        paddingHorizontal: helper.normalize(15),
        paddingVertical: helper.normalize(10),
        // paddingVertical: 4,
        // alignItems: "center",
    },
    textInput: {
        flex: 1,
        padding: 0,
        fontSize: helper.normalize(16),
        // color: '#05375a',
        fontFamily: themeStyle.POPPINS_REGULAR
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: helper.normalize(10),
        // padding: 6,
        opacity: 0.6,
        // position: "absolute",
        fontFamily: themeStyle.POPPINS_REGULAR,
    },
    signIn: {
        width: '100%',
        paddingVertical: helper.normalize(10),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: helper.normalize(6),
    },
    textSign: {
        fontSize: helper.normalize(16),
        fontFamily: themeStyle.POPPINS_MEDIUM,

    },
    iconStyle: {
        opacity: 1,
        color: themeStyle.PRIMARY_COLOR,
        fontSize: helper.normalize(14)
    },
    input: {
        fontFamily: themeStyle.POPPINS_REGULAR,
        fontSize: helper.normalize(14),
        borderColor: "grey",
        borderWidth: 0.2,
        borderRadius: 2,
        paddingLeft: 10,
    },
});
