import React, { useEffect, useContext, useState, useRef } from 'react';
import {View, Text, TouchableOpacity, Dimensions, StyleSheet, StatusBar, Image,} from 'react-native';
import helper from "../components/helper";
import themeStyle from '../styles/theme.style';
import Swiper from 'react-native-swiper';
import RNBootSplash from "react-native-bootsplash";

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const SplashScreen = ({ navigation }) => {

    const [activeIndex, setActiveIndex] = useState(0);

    const swiperRef = useRef(null);

    useEffect(() => {
        RNBootSplash.hide();
    });

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='rgba(0,0,0,0)' barStyle="dark-content" translucent={true} />
            <Swiper
                ref={swiperRef}
                onMomentumScrollEnd={(e, state, context) =>
                    console.log('index:', state.index)
                }
                dot={
                    <View
                        style={{
                            backgroundColor: "blue",
                            opacity: 0.4,
                            width: 5,
                            height: 5,
                            borderRadius: 4,
                            marginLeft: 3,
                            marginRight: 3,
                        }}
                    />
                }
                index={activeIndex}
                activeDot={
                    <View
                        style={{
                            backgroundColor: "blue",
                            width: 8,
                            height: 8,
                            borderRadius: 4,
                            marginLeft: 3,
                            marginRight: 3,
                        }}
                    />
                }
                paginationStyle={{
                }}
                loop={false}
            >
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'flex-start',
                        backgroundColor: 'transparent'
                    }}
                >
                    <Image
                        resizeMode="contain"
                        style={styles.image}
                        source={require('../assets/swiper1.png')}
                    />
                    <View style={{position: "absolute", alignItems: "center", bottom: helper.normalize(90), width: screenWidth}}>
                        <Text style={{fontFamily: themeStyle.POPPINS_BOLD, color: "white", fontSize: helper.normalize(16)}} numberOfLines={1}>BosCOD.com</Text>
                    </View>
                    <TouchableOpacity
                        style={{
                            overflow:'hidden', 
                            position: "absolute",
                            bottom: helper.normalize(50),
                            width: "95%", 
                            backgroundColor: "white", 
                            alignItems: "center",
                            justifyContent: "center",
                            marginLeft: helper.normalize(11),
                            height: helper.normalize(35),
                            borderRadius: helper.normalize(20),
                        }}
                        onPress={() => { swiperRef.current.scrollBy(1)}}
                    >
                         <Text style={styles.textSwiper} numberOfLines={1}>Lanjut</Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'flex-start',
                        backgroundColor: 'transparent'
                    }}
                >
                    <Image
                        resizeMode="contain"
                        style={styles.image}
                        source={require('../assets/swiper2.png')}
                    />
                    <View style={{ flex: 0.6, paddingHorizontal: helper.normalize(60), paddingTop: helper.normalize(30), alignItems: "center" }}>
                        <Text style={styles.textTitle} numberOfLines={1}>Kirim Paket</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>COD / Non-COD</Text>
                        <Text style={[styles.textDescription,{paddingTop:20}]} >Anytime Anywhere</Text>
                        <Text style={styles.textDescription} >On Your Hand</Text>
                    </View>
                    <TouchableOpacity
                        style={{
                            overflow:'hidden', 
                            position: "absolute",
                            bottom: helper.normalize(50),
                            width: "95%", 
                            backgroundColor: "orange", 
                            alignItems: "center",
                            justifyContent: "center",
                            marginLeft: helper.normalize(11),
                            height: helper.normalize(35),
                            borderRadius: helper.normalize(20),
                        }}
                        onPress={() => { swiperRef.current.scrollBy(1)}}
                    >
                         <Text style={styles.textSwiper2} numberOfLines={1}>Lanjut</Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'flex-start',
                        backgroundColor: 'transparent'
                    }}
                >
                    <Image
                        resizeMode="contain"
                        style={styles.image}
                        source={require('../assets/swiper3.png')}
                    />
                    <View style={{ flex: 0.6, paddingHorizontal: helper.normalize(60), paddingTop: helper.normalize(30), alignItems: "center" }}>
                        <Text style={styles.textTitle} numberOfLines={1}>Kiriman Lancar</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>Bisnis Jadi Besar</Text>
                        <Text style={[styles.textDescription,{paddingTop:20}]} >Scale Up Your Bisnis</Text>
                        <Text style={styles.textDescription} >With BosCOD</Text>
                    </View>
                    <TouchableOpacity
                        style={{
                            overflow:'hidden', 
                            position: "absolute",
                            bottom: helper.normalize(50),
                            width: "95%", 
                            backgroundColor: "orange", 
                            alignItems: "center",
                            justifyContent: "center",
                            marginLeft: helper.normalize(11),
                            height: helper.normalize(35),
                            borderRadius: helper.normalize(20),
                        }}
                        onPress={() => navigation.navigate('SignInScreen')}
                    >
                         <Text style={styles.textSwiper2} numberOfLines={1}>Mulai</Text>
                    </TouchableOpacity>
                </View>
            </Swiper>
        </View>
    );
};

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#EF5E2F',
        backgroundColor: '#FFF',
    },
    textSign: {
        color: themeStyle.PRIMARY_COLOR,
        fontFamily: themeStyle.POPPINS_SEMIBOLD,
        fontSize: helper.normalize(14),
    },
    image: {
        width: screenWidth,
        flex: 1,
        borderWidth: 1,
        borderColor: "rgba(0,0,0,0)"
    },
    textTitle: { fontFamily: themeStyle.POPPINS_BOLD, color: "black", fontSize: helper.normalize(25) },
    textSwiper: { fontFamily: themeStyle.POPPINS_BOLD, color: "grey", fontSize: helper.normalize(20) },
    textSwiper2: { fontFamily: themeStyle.POPPINS_BOLD, color: "black", fontSize: helper.normalize(20) },
    textDescription: { fontFamily: themeStyle.POPPINS_REGULAR, color: "#000", fontSize: helper.normalize(14), textAlign: "center" }
});

