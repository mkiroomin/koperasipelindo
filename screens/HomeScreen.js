import React, { useEffect, useRef, useState, useCallback, useMemo } from 'react';
import {View, Text, StyleSheet, StatusBar, Dimensions, Image, TouchableOpacity, TextInput, Linking, RefreshControl, ImageBackground, Alert, Animated, ScrollView} from 'react-native';
import helper from '../components/helper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { GoogleSignin, GoogleSigninButton, statusCodes, } from '@react-native-community/google-signin';
import Dialog, { DialogContent } from 'react-native-popup-dialog';


const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const HomeScreen = ({ navigation }) => {

  useEffect(() => {
    mounted.current = true;
    StatusBar.setBackgroundColor("rgba(255,255,255,0)")
    StatusBar.setBarStyle("dark-content")
    StatusBar.setTranslucent(true);
    let componentMounted = true;

    // console.log("popopopopo ", datak2)
    getTransaksi();
    getListPengajuan()


    if (componentMounted) {
      // console.log("=====>popo", plafonEdit)
      getUserData();
      getSaldo();
    }

    return () => {
      componentMounted = false;
      mounted.current = false;
    }
  }, [])

  const mounted = useRef(true);
  const [datak, setDatak] = useState(0);
  const [plafon, setplafon] = useState("");
  const [tenor, setTenor] = useState("");
  const [id, setid] = useState("");
  const [datak2, setDatak2] = useState([]);
  const [list, setList] = useState([]);
  const [openAlert, setOpenAlert] = useState(false);
  const [openAlertEdit, setOpenAlertEdit] = useState(false);
  const [platformEdit, setplafonedit] = useState("plafon");
  const [tenorEdit, setTenoredit] = useState("tenor");

  const getUserData = () => {
    (async () => {
      let userName;
      let email;
      userName = await AsyncStorage.getItem('@name');
      email = await AsyncStorage.getItem('@email');
      console.log(userName, email)
    })();
  }

  const open = () => {
    setOpenAlert(true)
  }

  const _signOut = async () => {
      GoogleSignin.configure({
        webClientId: '40286503666-qm2qmupib27mj1d34vcmbsi2452lnoti.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
        offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        forceCodeForRefreshToken: false, // [Android] related to `serverAuthCode`, read the docs link below *.
      });

      await GoogleSignin.signOut();

      await AsyncStorage.removeItem('@name')
      await AsyncStorage.removeItem('@email')
      
      navigation.navigate('SignInScreen');
  };

  const getSaldo = async () => {
    try {

      const req = await fetch("http://13.250.8.152:8090/saldo.json", {
        method: 'GET',
        dataType: 'json',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          // 'Authorization': 'Bearer ' + access_token,
        },
      });
      const datas = await req.json();
      setDatak(datas.balance);
    } catch (err) {
    }
  };

  const getTransaksi = async () => {
    try {

      const req = await fetch("http://13.250.8.152:8090/transaksi.json", {
        method: 'GET',
        dataType: 'json',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          // 'Authorization': 'Bearer ' + access_token,
        },
      });
      const datas2 = await req.json();
      console.log("lololol", datas2.data);
      setDatak2(datas2.data);
    } catch (err) {
    }
  };

  const getListPengajuan = async () => {
    try {

      const req = await fetch("https://test1.kopelindo.co.id/api/pengajuan/list", {
        method: 'GET',
        dataType: 'json',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          // 'Authorization': 'Bearer ' + access_token,
        },
      });
      const data = await req.json();
      // console.log("lololol", datas2.data);
      setList(data);
    } catch (err) {
    }
  };

  const deleteList = async (id) => {
    console.log(id)
    try {
      const req = await fetch("https://test1.kopelindo.co.id/api/pengajuan/delete/"+id, {
        method: 'DELETE',
        dataType: 'json',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
          // 'Authorization': 'Bearer ' + access_token,
        },
      });
      const data = await req.json();
      console.log("popo", data);
      // setDatak2(datas2.data)
      if(data.status == "success"){
        getListPengajuan();
      }
    } catch (err) {
      console.log(err)
    }
  };

  const saveButton = async () => {
    try {
        const request = await fetch("https://test1.kopelindo.co.id/api/pengajuan/create", {
            method: 'POST',
            dataType: 'json',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            },
            body: JSON.stringify({
              plafon: plafon,
              tenor: tenor,
            })
        })

        const response = await request.json()
        // console.log('response REGION POST', response)
        if (response.status == "success") {
            setOpenAlert(false);
            getListPengajuan();
        }
    } catch (error) {
    }
  };

  const saveButtonEdit = async () => {
    try {
        const request = await fetch("https://test1.kopelindo.co.id/api/pengajuan/update/"+id, {
            method: 'POST',
            dataType: 'json',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache',
            },
            body: JSON.stringify({
              plafon: platformEdit,
              tenor: tenorEdit,
            })
        })

        const response = await request.json()
        console.log('response REGION POST', response)
        if (response.status == "success") {
            setOpenAlertEdit(false);
            getListPengajuan();
        }
    } catch (error) {
    }
  };

  const editList = async (id, plafonnew, tenornew) => {
    setplafonedit(plafonnew);
    setTenoredit(tenornew);
    setid(id);
    setOpenAlertEdit(true);
  }

  return (
    <View
      style={[styles.container, { backgroundColor: '#FFFFFF' }]}
    >
      <Dialog
        visible={openAlert}
        onTouchOutside={() => {
          setOpenAlert(false);
        }}
        onHardwareBackPress={() => {
          setOpenAlert(false);
            return true;
        }}
      >
        <DialogContent>
          <View style={{width: screenWidth - 100}}>
              <View style={{ justifyContent: 'center', alignItems: "center", paddingTop: helper.normalize(20),paddingBottom: 10 }}>
                  <Text style={styles.textCard2}>Tambah Pengajuan </Text>
              </View>
              <View style={[styles.action, { borderTopRightRadius: 0, borderTopLeftRadius: 0, }]}>
                <View style={{ flexDirection: "row" }} >
                    <TextInput

                        placeholder="Plafon"
                        keyboardType="numeric"
                        placeholderTextColor="#CCC"
                        // secureTextEntry={data.secureTextEntry ? true : false}
                        style={[styles.textInput, {
                            color: "black"
                        }]}
                        autoCapitalize="none"
                        onChangeText={(val) => setplafon(val)}
                    />
                </View>
            </View>

            <View style={[styles.action, { borderTopRightRadius: 0, borderTopLeftRadius: 0, marginTop: 10 }]}>
                <View style={{ flexDirection: "row" }} >
                    <TextInput

                        placeholder="Tenor"
                        keyboardType="numeric"
                        placeholderTextColor="#CCC"
                        // secureTextEntry={data.secureTextEntry ? true : false}
                        style={[styles.textInput, {
                            color: "black"
                        }]}
                        autoCapitalize="none"
                        onChangeText={(val) => setTenor(val)}
                    />
                </View>
            </View>
            <TouchableOpacity
                        style={{
                            // position: "absolute",
                            // bottom: helper.normalize(15),
                            // width: "80%", 
                            backgroundColor: "blue", 
                            alignItems: "center",
                            justifyContent: "center",
                            marginTop: 15
                            // height: helper.normalize(35),
                            // borderRadius: helper.normalize(10),
                            // opacity:0.9
                        }}
                        onPress={() => saveButton()}
                        >
                        <Text style={styles.textBuySave} numberOfLines={1}>Save</Text>
            </TouchableOpacity>
              {/* {
                datak2?.map((item)=> 
                  <View style={{ justifyContent: 'center', paddingTop: helper.normalize(20)}}>
                    <TouchableOpacity style={styles.card2}>
                      <View>
                        <Text style={styles.textCard2}>{item.product_name}</Text>
                        <Text style={styles.textCard3}>{item.created_on}</Text>
                      </View>
                      <Text style={[styles.textCard5, {paddingRight: 15}]}>Rp {item.product_price}</Text>
                    </TouchableOpacity>
                  </View>
                )            
              } */}
            </View>
        </DialogContent>
      </Dialog>

      <Dialog
        visible={openAlertEdit}
        onTouchOutside={() => {
          setOpenAlertEdit(false);
        }}
        onHardwareBackPress={() => {
          setOpenAlertEdit(false);
            return true;
        }}
      >
        <DialogContent>
          <View style={{width: screenWidth - 100}}>
              <View style={{ justifyContent: 'center', alignItems: "center", paddingTop: helper.normalize(20),paddingBottom: 10 }}>
                  <Text style={styles.textCard2}>Tambah Pengajuan </Text>
              </View>
              <View style={[styles.action, { borderTopRightRadius: 0, borderTopLeftRadius: 0, }]}>
                <View style={{ flexDirection: "row" }} >
                    <TextInput
                        value= {platformEdit}
                        placeholder="Plafon"
                        keyboardType="numeric"
                        placeholderTextColor="#CCC"
                        // secureTextEntry={data.secureTextEntry ? true : false}
                        style={[styles.textInput, {
                            color: "black"
                        }]}
                        // autoCapitalize="none"
                        onChangeText={(val) => setplafonedit(val)}
                    />
                </View>
            </View>

            <View style={[styles.action, { borderTopRightRadius: 0, borderTopLeftRadius: 0, marginTop: 10 }]}>
                <View style={{ flexDirection: "row" }} >
                    <TextInput

                        placeholder="Tenor"
                        keyboardType="numeric"
                        placeholderTextColor="#CCC"
                        // secureTextEntry={data.secureTextEntry ? true : false}
                        style={[styles.textInput, {
                            color: "black"
                        }]}
                        autoCapitalize="none"
                        value={tenorEdit}
                        onChangeText={(val) => setTenoredit(val)}
                    />
                </View>
            </View>
            <TouchableOpacity
                        style={{
                            // position: "absolute",
                            // bottom: helper.normalize(15),
                            // width: "80%", 
                            backgroundColor: "blue", 
                            alignItems: "center",
                            justifyContent: "center",
                            marginTop: 15
                            // height: helper.normalize(35),
                            // borderRadius: helper.normalize(10),
                            // opacity:0.9
                        }}
                        onPress={() => saveButtonEdit()}
                        >
                        <Text style={styles.textBuySave} numberOfLines={1}>Save</Text>
            </TouchableOpacity>
              {/* {
                datak2?.map((item)=> 
                  <View style={{ justifyContent: 'center', paddingTop: helper.normalize(20)}}>
                    <TouchableOpacity style={styles.card2}>
                      <View>
                        <Text style={styles.textCard2}>{item.product_name}</Text>
                        <Text style={styles.textCard3}>{item.created_on}</Text>
                      </View>
                      <Text style={[styles.textCard5, {paddingRight: 15}]}>Rp {item.product_price}</Text>
                    </TouchableOpacity>
                  </View>
                )            
              } */}
            </View>
        </DialogContent>
      </Dialog>

      <StatusBar
        animated={true}
        backgroundColor={"rgba(0,0,0,0)"}
        barStyle={'dark-content'}
        showHideTransition='slide'
      />
      <View >
        <ImageBackground
          source={require("../assets/header_image.png")}
          resizeMode={'cover'}
          style={{
            position: "absolute",
            top: 0,
            flex: 1, 
            width: screenWidth,
            height: 350
          }}
        >
          <TouchableOpacity>
            <Ionicons 
              name="menu-outline"
              color="white"
              size={helper.normalize(30)}
              style={{
                position: "absolute",
                top: helper.normalize(50), 
                left: helper.normalize(20)}}
              />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => 
            Alert.alert(
              'LogOut',
              'Apakah anda yakin ingin keluar?', // <- this part is optional, you can pass an empty string
              [
                {text: 'OK', onPress: () => _signOut()},
                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
              ],
              {cancelable: false},
            )
          }>

            <Ionicons 
              name="person-circle-outline"
              color="white"
              size={helper.normalize(30)}
              style={{
                position: "absolute",
                top: helper.normalize(50), 
                right: helper.normalize(20)}}
              />
          </TouchableOpacity>

          <View style={{flexDirection: "row", justifyContent: 'space-between', paddingTop: helper.normalize(110), paddingHorizontal: helper.normalize(25)}}>
          <Text style={{color:"white", fontSize: helper.normalize(25), fontWeight: 'bold'}}>List Pengajuan</Text>
            <TouchableOpacity 
              style={{backgroundColor:"white", borderRadius: 5, width: helper.normalize(70), alignItems: "center", justifyContent: "center"}}
              // onPress={open()}
              onPress={() => { open()}}
              >
              <Text style={{color:"#483D8B", fontSize: helper.normalize(16), fontWeight: 'bold'}}>Tambah</Text>
            </TouchableOpacity>
          </View>
          <View style={{paddingTop: helper.normalize(20), paddingHorizontal: helper.normalize(25),}}>
          {/* <View style={{paddingTop: helper.normalize(20), paddingHorizontal: helper.normalize(25)}}> */}
            {/* <Text style={{color:"white", fontSize: helper.normalize(22), fontWeight: 'bold'}}>All features</Text>
            <View style={{flexDirection: "row", justifyContent: 'space-between', paddingTop: helper.normalize(20)}}>
              <TouchableOpacity 
                style={styles.card}
                >
                <Image
                    style={{
                        width: 60,
                        height: 60,
                        borderRadius: 15
                    }}
                    source={require("../assets/melon.jpeg")}
                    resizeMode="cover"
                />
                <Text style={styles.textCard}>Top Up</Text>
              </TouchableOpacity>
              <TouchableOpacity 
                style={styles.card}
                >
                <Image
                    style={{
                        width: 60,
                        height: 60,
                        borderRadius: 15
                    }}
                    source={require("../assets/paket.jpeg")}
                    resizeMode="cover"
                />
                <Text style={styles.textCard}>Withdraw</Text>
              </TouchableOpacity>
            </View>

            <View style={{flexDirection: "row", justifyContent: 'space-between', paddingTop: helper.normalize(20)}}>
              <TouchableOpacity 
                style={styles.card}
                >
                <Image
                    style={{
                        width: 60,
                        height: 60,
                        borderRadius: 15
                    }}
                    source={require("../assets/sayur.jpeg")}
                    resizeMode="cover"
                />
                <Text style={styles.textCard}>Send</Text>
              </TouchableOpacity>
              <TouchableOpacity 
                style={styles.card}
                >
                <Image
                    style={{
                        width: 60,
                        height: 60,
                        borderRadius: 15
                    }}
                    source={require("../assets/frozen.jpeg")}
                    resizeMode="cover"
                />
                <Text style={styles.textCard}>Pay</Text>
              </TouchableOpacity>
            </View> */}
            {/* <Text style={{color:"black", fontSize: helper.normalize(25), fontWeight: 'bold', paddingTop: helper.normalize(30)}}>RECENT TRANSACTIONS</Text> */}
            {/* <View style={{ justifyContent: 'center', paddingTop: helper.normalize(10)}}>
              <TouchableOpacity style={styles.card2}>
                  <View style={{flexDirection: "row"}}>
                    <Image
                        style={{
                            width: 30,
                            height: 30,
                            borderRadius: 5,
                            // marginLeft: helper.normalize(20)
                        }}
                        source={require("../assets/sayur.jpeg")}
                        resizeMode="cover"
                    />
                    <View>
                      <Text style={styles.textCard2}>Received from PhonePe</Text>
                      <Text style={styles.textCard3}>3:00 PM</Text>
                    </View>
                  </View>
                  <Text style={styles.textCard4}>Rp 1500</Text>
                </TouchableOpacity>
            </View>
            <View style={{ justifyContent: 'center', paddingTop: helper.normalize(10)}}>
              <TouchableOpacity style={styles.card2}>
                  <View style={{flexDirection: "row"}}>
                    <Image
                        style={{
                            width: 30,
                            height: 30,
                            borderRadius: 5,
                            // marginLeft: helper.normalize(20)
                        }}
                        source={require("../assets/sayur.jpeg")}
                        resizeMode="cover"
                    />
                    <View>
                      <Text style={styles.textCard2}>Flaticon</Text>
                      <Text style={styles.textCard3}>4:00 PM</Text>
                    </View>
                  </View>
                  <Text style={styles.textCard5}>Rp-1200</Text>
                </TouchableOpacity>
            </View> */}
            <ScrollView style={{width: screenWidth, height: screenHeight}}>
            {
              list?.map((item)=> 
                <View style={{paddingTop: helper.normalize(20)}}>
                  <View style={styles.card2Pelabuhan}>
                    <View style={{ flexDirection: "row",paddingTop: 15 }}>
                      <View>
                        <Text style={styles.textCard2Pelabuhan}>Plafon </Text>
                        <Text style={styles.textCard2Pelabuhan}>tenor </Text>
                      </View>
                      <View>
                        <Text style={styles.textCard2Pelabuhan}>: Rp. {item.plafon} </Text>
                        <Text style={styles.textCard2Pelabuhan}>: {item.tenor} Tahun</Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: "row",paddingTop: 15, paddingBottom: 15, paddingLeft: 150}}>
                      <TouchableOpacity
                        style={{
                            // position: "absolute",
                            // bottom: helper.normalize(15),
                            // width: "80%", 
                            backgroundColor: "blue", 
                            alignItems: "center",
                            justifyContent: "center",
                            // height: helper.normalize(35),
                            borderRadius: helper.normalize(10),
                            // opacity:0.9
                        }}
                        onPress={() => editList(item.id, item.plafon, item.tenor)}
                        >
                        <Text style={styles.textBuy} numberOfLines={1}>Edit</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={{
                            // position: "absolute",
                            // bottom: helper.normalize(15),
                            // width: "80%", 
                            marginLeft: 10,
                            backgroundColor: "blue", 
                            alignItems: "center",
                            justifyContent: "center",
                            // height: helper.normalize(35),
                            borderRadius: helper.normalize(10),
                            // opacity:0.9
                        }}
                        onPress={() => deleteList(item.id)}
                        >
                        <Text style={styles.textBuy} numberOfLines={1}>Hapus</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              )            
            }
            </ScrollView>
          </View>
        </ImageBackground>
      </View>
    </View >
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    backgroundColor:"white", 
    borderRadius: 20, 
    width: helper.normalize(135), 
    height: helper.normalize(130),
    alignItems: "center", 
    justifyContent: "center",
    elevation: 20,
    shadowColor: 'black',
    shadowOffset: {width: -2, height: 90},
    shadowRadius: 3,
  },
  textCard: {
    paddingTop: 20,
    color:"black", 
    fontSize: helper.normalize(20), 
    fontWeight: 'bold',
  },
  card2: {
    flexDirection: "row",
    justifyContent: 'space-between',
    backgroundColor:"white", 
    borderRadius: 5, 
    width: screenWidth - 50, 
    height: helper.normalize(50),
    alignItems: "center", 
    // justifyContent: "center",
    elevation: 20,
    shadowColor: 'black',
    shadowOffset: {width: -2, height: 90},
    shadowRadius: 3,
    paddingHorizontal: helper.normalize(18)
  },
  card2Pelabuhan: {
    // flexDirection: "row",
    // justifyContent: 'space-between',
    backgroundColor:"white", 
    borderRadius: 5, 
    width: screenWidth - 50, 
    // height: helper.normalize(70),
    // alignItems: "center", 
    // justifyContent: "center",
    elevation: 20,
    shadowColor: 'black',
    shadowOffset: {width: -2, height: 90},
    shadowRadius: 3,
    paddingHorizontal: helper.normalize(18)
  },
  textCard2: {
    color:"black", 
    paddingLeft: 20,
    fontSize: helper.normalize(18), 
    fontWeight: 'bold',
  },
  textCard2Pelabuhan: {
    color:"black", 
    paddingLeft: 10,
    fontSize: helper.normalize(18), 
    fontWeight: 'bold',
    padding: 2
  },
  textCard3: {
    color:"grey", 
    paddingLeft: helper.normalize(18),
  },
  textCard4: {
    color:"green", 
    // paddingLeft: helper.normalize(35),
    fontSize: helper.normalize(18), 
    fontWeight: 'bold',
  },
  textCard5: {
    color:"red", 
    // paddingLeft: helper.normalize(35),
    fontSize: helper.normalize(18), 
    fontWeight: 'bold',
  },
  textBuy: {color: "white", fontSize: helper.normalize(16), padding: 5, paddingHorizontal: 20},
  textBuySave : {color: "white", fontSize: helper.normalize(16), padding: 10, paddingHorizontal: 20},
  textInput: {
    fontSize: helper.normalize(16),
    // color: '#05375a',
  },
  action: {
    // flexDirection: 'row',
    // marginTop: 10,
    // borderBottomWidth: 1,
    borderWidth: 1,
    backgroundColor: "#FDFDFD",
    borderRadius: helper.normalize(6),
    borderColor: "#BBBBBB",
    // paddingHorizontal: helper.normalize(15),
    // paddingVertical: helper.normalize(10),
    // paddingVertical: 4,
    // alignItems: "center",
},
});

export default HomeScreen;