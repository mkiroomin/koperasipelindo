import React  from 'react';
import { connect } from 'react-redux';

import { createNativeStackNavigator } from '@react-navigation/native-stack';

import SplashScreen from './SplashScreen';
import SignInScreen from './SignInScreen';
import HomeScreen from './HomeScreen';

const RootStack = createNativeStackNavigator();


const RootStackScreen = ({ navigation, initialRouteName }) => {

    return (
        <RootStack.Navigator
            headerMode='none'
            initialRouteName={initialRouteName}
        // initialRouteName={initialRouteName}
        >
            <RootStack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{
                    headerShown: false,
                }} />
            <RootStack.Screen
                name="SignInScreen"
                component={SignInScreen}
                options={{
                    headerShown: false,
                }} />
            <RootStack.Screen
                name="HomeScreen"
                component={HomeScreen}
                options={{
                    headerShown: false,
                }} />
        </RootStack.Navigator>

    )

};


const mapStateToProps = (state) => {
    return {
        initialRouteName: state.InitialRouteRootStack.initialRouteName,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setInitialRouteName: (initialRouteName) => dispatch({ type: 'SET_INITIAL_ROUTE_NAME', initialRouteName: initialRouteName }),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(React.memo(RootStackScreen));