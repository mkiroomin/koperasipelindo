import React from "react"
import { Dimensions } from "react-native"
import ContentLoader, { Rect, Circle, Path, } from "react-content-loader/native"
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
const LoaderImage = (props) => (
    <ContentLoader
        speed={1}
        // width={screenWidth}
        // height={300}
        // viewBox="0 0 400 320"
        backgroundColor="#f9fbfd"
        foregroundColor="#EEEEEE"
        {...props}
    >
        <Rect x="0" y="5" rx="8" ry="8" width="190" height="285" />
    </ContentLoader>
)

export default LoaderImage;