import React from "react"
import { Dimensions } from "react-native"
import ContentLoader, { Rect, Circle, Path, } from "react-content-loader/native"
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
const LoaderCard = (props) => (
    <ContentLoader
        speed={1}
        width={screenWidth}
        height={100}
        backgroundColor="#f9fbfd"
        foregroundColor="#EEEEEE"
        {...props}
    >
        <Rect x="20" y="5" rx="8" ry="8" width={screenWidth / 5.5} height={screenWidth / 6.5} />
    </ContentLoader>
)

export default LoaderCard;