import React from "react"
import { Dimensions, View, Image } from "react-native"

import helper from "../helper";
import ContentLoader, { Rect, Circle, Path, } from "react-content-loader/native"
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
const LoaderCarousel = (props) => (
    <ContentLoader
        speed={0.5}
        width={screenWidth}
        height={helper.normalize(220)}
        // viewBox="0 0 400 320"
        foregroundColor="#FFFFFF"
        backgroundColor="#F4F4F4"
        // backgroundColor={'#F1F1F1'}
        {...props}
    >
        {/* <Rect x={screenWidth / 2 + helper.normalize(5)} y={10} rx={helper.normalize(30)} ry={helper.normalize(30)} width={helper.normalize(30)} height={helper.normalize(30)} /> */}
        {/* LEFT LOADER */}


        <View style={{ backgroundColor: 'rgba(0,0,0,0.0085)', justifyContent: "center", alignItems: 'center', borderWidth: 1, borderColor: '#F1F1F1', marginLeft: helper.normalize(20), borderTopRightRadius: helper.normalize(12), borderTopLeftRadius: helper.normalize(12), height: helper.normalize(125), width: screenWidth / 2 - helper.normalize(30) }} >
            <Image source={require('../../assets/product_image.png')} style={{ opacity: 0.8, resizeMode: 'contain', height: helper.normalize(75), width: helper.normalize(75) }} />
        </View>
        <View style={{ backgroundColor: 'rgba(0,0,0,0.0085)', borderWidth: 1, borderTopWidth: 0,borderColor: '#F1F1F1', marginLeft: helper.normalize(20), borderBottomLeftRadius: helper.normalize(12), borderBottomRightRadius: helper.normalize(12), height: helper.normalize(100), width: screenWidth / 2 - helper.normalize(30) }} >
        </View>

        {/* <Rect
            x={helper.normalize(20)}
            y="0"
            rx={helper.normalize(16)}
            ry={helper.normalize(16)}
            width={screenWidth / 2 - helper.normalize(30)}
            height={helper.normalize(220)}
        /> */}
        <Rect
            x={helper.normalize(25)}
            y={helper.normalize(135)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(135)}
            height={helper.normalize(8)}
        />
        <Rect
            x={helper.normalize(25)}
            y={helper.normalize(145)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(60)}
            height={helper.normalize(12)}
        />
        <Rect
            x={helper.normalize(25)}
            y={helper.normalize(160)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={helper.normalize(15)}
            height={helper.normalize(15)}
        />
        <Rect
            x={helper.normalize(45)}
            y={helper.normalize(160)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(120)}
            height={helper.normalize(15)}
        />
        <Rect
            x={helper.normalize(25)}
            y={helper.normalize(185)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(100)}
            height={helper.normalize(8)}
        />
        <Rect
            x={screenWidth / 2 - helper.normalize(50)}
            y={helper.normalize(180)}
            rx={helper.normalize(35)}
            ry={helper.normalize(35)}
            width={helper.normalize(35)}
            height={helper.normalize(35)}
        />



        {/* RIGHT LOADER */}
        <View style={{ position: 'absolute', backgroundColor: 'rgba(0,0,0,0.0085)', justifyContent: "center", alignItems: 'center', borderWidth: 1, borderColor: '#F1F1F1', top: 0, left: screenWidth / 2 + helper.normalize(10), borderTopRightRadius: helper.normalize(12), borderTopLeftRadius: helper.normalize(12), height: helper.normalize(125), width: screenWidth / 2 - helper.normalize(30) }} >
            <Image source={require('../../assets/product_image.png')} style={{ opacity: 0.8, resizeMode: 'contain', height: helper.normalize(75), width: helper.normalize(75) }} />
        </View>
        <View style={{  position: 'absolute', backgroundColor: 'rgba(0,0,0,0.0085)', borderWidth: 1, borderTopWidth:0, borderColor: '#F1F1F1', left: screenWidth / 2 + helper.normalize(10), borderBottomLeftRadius: helper.normalize(12), borderBottomRightRadius: helper.normalize(12), top: helper.normalize(125),  height: helper.normalize(100), width: screenWidth / 2 - helper.normalize(30) }} >
        </View>
        {/* <Rect
            x={screenWidth / 2 + helper.normalize(10)}
            y="0"
            rx={helper.normalize(16)}
            ry={helper.normalize(16)}
            width={screenWidth / 2 - helper.normalize(30)}
            height={helper.normalize(220)}
        /> */}
        <Rect
            x={screenWidth / 2 + helper.normalize(15)}
            y={helper.normalize(135)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(135)}
            height={helper.normalize(8)}
        />
        <Rect
            x={screenWidth / 2 + helper.normalize(15)}
            y={helper.normalize(145)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(60)}
            height={helper.normalize(12)}
        />
        <Rect
            x={screenWidth / 2 + helper.normalize(15)}
            y={helper.normalize(160)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={helper.normalize(15)}
            height={helper.normalize(15)}
        />
        <Rect
            x={screenWidth / 2 + helper.normalize(35)}
            y={helper.normalize(160)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(120)}
            height={helper.normalize(15)}
        />
        <Rect
            x={screenWidth / 2 + helper.normalize(15)}
            y={helper.normalize(185)}
            rx={helper.normalize(6)}
            ry={helper.normalize(6)}
            width={screenWidth / 2 - helper.normalize(100)}
            height={helper.normalize(8)}
        />
        <Rect
            x={screenWidth - helper.normalize(60)}
            y={helper.normalize(180)}
            rx={helper.normalize(35)}
            ry={helper.normalize(35)}
            width={helper.normalize(35)}
            height={helper.normalize(35)}
        />


        {/* <Rect x={helper.normalize(20)} y={helper.normalize(260)} rx={helper.normalize(8)} ry={helper.normalize(8)} width={screenWidth / 2 - helper.normalize(30)} height={helper.normalize(240)} />
        <Rect x={screenWidth / 2 + helper.normalize(10)} y={helper.normalize(260)} rx={helper.normalize(8)} ry={helper.normalize(8)} width={screenWidth / 2 - helper.normalize(30)} height={helper.normalize(240)} /> */}
        {/* <Rect x="0" y="5" rx="12" ry="12" width="205" height="310" />
        <Rect x="215" y="15" rx="12" ry="12" width="185" height="290" /> */}
    </ContentLoader>
)

export default LoaderCarousel;