import React from "react"
import { Dimensions } from "react-native"
import ContentLoader, { Rect, Circle, Path, } from "react-content-loader/native"
import helper from "../helper";
import themeStyle from "../../styles/theme.style";

const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);

const LoaderButtonAdd = (props) => (
    <ContentLoader
        speed={0.5}
        width={helper.normalize(34)}
        height={helper.normalize(34)}
        backgroundColor="#EEEEEE"
        foregroundColor={"#FAFAFA"}
        {...props}
    >
        <Rect x={0} y={0} rx={helper.normalize(30)} ry={helper.normalize(30)} height={helper.normalize(34)} width={helper.normalize(34)} />
        {/* <Rect x={(helper.normalize(17)) / 2} y={(helper.normalize(34) / 2) - helper.normalize(6)} rx={helper.normalize(5)} ry={helper.normalize(5)} height={helper.normalize(5)} width={helper.normalize(34) / 2} />
        <Rect x={(helper.normalize(34) / 2) - helper.normalize(6)} y={helper.normalize(17) /2} rx={helper.normalize(5)} ry={helper.normalize(5)} height={helper.normalize(34) / 2} width={helper.normalize(5)} /> */}
    </ContentLoader>
)

export default LoaderButtonAdd;