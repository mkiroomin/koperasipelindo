import React from "react"
import { Dimensions } from "react-native"
import ContentLoader, { Rect, Circle, Path, } from "react-content-loader/native"
import helper from "../helper";

const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);

const LoaderCarousel = (props) => (
    <ContentLoader
        speed={1}
        width={screenWidth}
        height={helper.normalize(40)}
        foregroundColor="#FFF"
        backgroundColor="#EEE"
        
        {...props}
    >
        <Rect x={helper.normalize(20)} y={helper.normalize(10)} rx={helper.normalize(6)} ry={helper.normalize(6)} height={helper.normalize(14)} width={screenWidth / 4} />
    </ContentLoader>
)

export default LoaderCarousel;