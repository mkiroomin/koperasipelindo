import React, { memo } from 'react';
import PropTypes from 'prop-types';
import {
    StyleSheet,
    Text,
    Touchable,
    View,
    Image,
    ImageBackground,
    Dimensions,
    Animated,
    // TouchableNativeFeedback,
    Pressable,
    // TouchableOpacity,
    Platform,
} from 'react-native';
// import helper from "../components/helper";
// import * as Animatable from 'react-native-animatable';
// import LinearGradient from 'react-native-linear-gradient';
import { TouchableNativeFeedback, TouchableHighlight, TouchableWithoutFeedback, TouchableOpacity } from 'react-native-gesture-handler';
// import theme from "../styles/theme.style";
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const TouchableNative = ({
    onPress,
    // onPressIn,
    onLongPress,
    style,
    disabled,
    children,
    key,
}) => {
    const scaleInAnimated = new Animated.Value(0);
    const scaleOutAnimated = new Animated.Value(0);

    if (Platform.OS == "android") {
        return (
            <View
                key={key}
                style={{ ...style }}
            >
                <TouchableHighlight
                    key={key}
                    hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
                    useForeground={true}
                    activeOpacity={0.6}
                    underlayColor="#EEEEEE"
                    // background={TouchableNativeFeedback.Ripple('rgba(0,0,0,0.050)', true)}
                    // onPressIn={() => { SCALE.pressInAnimation(scaleOutAnimated); }}
                    // onPressOut={() => { SCALE.pressOutAnimation(scaleOutAnimated); }}
                    // style={SCALE.getScaleTransformationStyle(scaleOutAnimated)}
                    // background={TouchableNativeFeedback.SelectableBackgroundBorderless()}
                    onPress={onPress}
                    // delayPressIn={0}
                    // onLongPress={onLongPress}
                    // delayLongPress={1000}
                    disabled={disabled}
                // disallowInterruption={true}
                >
                    {children}
                </TouchableHighlight>

            </View>

        );
    } else {
        return (
            <Animated.View
                key={key}
                style={{ ...style }}
            >
                {onPress ?
                    <TouchableOpacity
                        key={key}
                        // hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                        activeOpacity={0.6}
                        onPress={onPress}
                        // onLongPress={onLongPress}
                        // onPressIn={() => { SCALE.pressInAnimation(scaleOutAnimated); }}
                        // onPressOut={() => { SCALE.pressOutAnimation(scaleOutAnimated); }}
                        // style={SCALE.getScaleTransformationStyle(scaleOutAnimated)}
                        // delayLongPress={1000}
                        disabled={disabled}
                    // delayPressIn={0}
                    >
                        {children}
                    </TouchableOpacity>
                    :
                    <TouchableOpacity
                        key={key}
                        activeOpacity={0.6}
                        // hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                        // onPressIn={() => { SCALE.pressInAnimation(scaleOutAnimated); }}
                        // onPressOut={() => { SCALE.pressOutAnimation(scaleOutAnimated); }}
                        // style={SCALE.getScaleTransformationStyle(scaleOutAnimated)}
                        // onPressIn={onPress}
                        // onLongPress={onLongPress}
                        // delayPressIn={0}
                        disabled={disabled}
                    >
                        {children}
                    </TouchableOpacity>
                }
            </Animated.View>

        );

    }
}

const SCALE = {
    // this defines the terms of our scaling animation. 
    getScaleTransformationStyle(animated = Animated.Value, startSize = 1, endSize = 0.99) {
        const interpolation = animated.interpolate({
            inputRange: [0, 1],
            outputRange: [startSize, endSize],
        });
        return {
            transform: [
                { scale: interpolation },
            ],
        };
    },
    // This defines animation behavior we expext onPressIn
    pressInAnimation(animated = Animated.Value, duration = 150) {
        animated.setValue(0);
        Animated.timing(animated, {
            toValue: 1,
            duration,
            useNativeDriver: true,
        }).start();
    },
    // This defines animatiom behavior we expect onPressOut
    pressOutAnimation(animated = Animated.Value, duration = 150) {
        animated.setValue(1);
        Animated.timing(animated, {
            toValue: 0,
            duration,
            useNativeDriver: true,
        }).start();
    },
};

TouchableNative.propTypes = {
    key: PropTypes.any,
    onPress: PropTypes.func,
    onPressIn: PropTypes.func,
    style: PropTypes.any,
    children: PropTypes.element.isRequired,
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});



export default memo(TouchableNative);